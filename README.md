Module demonstrates how to use the CTools plugins and styles:

1. Module creates CTools content Plugin for panels to show 3 random published nodes that was created on the site in 'teaser' view mode. 

2. Module creates a Panels Style plugin (both for pane and region) that will show content in a border with a width specified through the style settings.

This module is for Drupal 7.